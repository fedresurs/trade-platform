import asyncio
from core import state
from schema import Status
from browser import Browser
from loguru import logger
from datetime import datetime
from spiders.purchase import SpiderPurchase
import orjson


async def worker(name):

    platform = state.get(name)
    total = 0
    async with Browser(name, platform) as browser:
        page = await browser.new_page()
        spider_purchase = SpiderPurchase(platform, page)
        await spider_purchase.start()
        async for item in spider_purchase.parse():
            item = orjson.dumps(item)
            # total += 1
            # print(name, '', total)
        platform.last_update = datetime.now()
        state.set_state(name, platform)
        return 'Success'


async def main(names: list):

    for name in names:
        platform = state.get(name)

        if platform.status == Status.disabled:
            logger.info(f'Disabled *- {name} -*')
            continue

        delta = datetime.now() - platform.last_update
        if delta.seconds // 60 < 10:
            logger.info(f'The data *- {name} -* for does not require updating')
            continue

        if platform.process:
            logger.info(f'the parsing *- {name} -* process is already in progress')
            continue

        await worker(name)




