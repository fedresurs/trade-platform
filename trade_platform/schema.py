from datetime import datetime
from pydantic import BaseModel
from enum import Enum


class Status(str, Enum):
    enabled = 'enabled'
    disabled = 'disabled'


class Platform(BaseModel):
    process: bool = False
    status: Status
    main_url: str
    start_url: str
    slow_mo: int = None
    last_update: datetime
    limit_page: int = 999
