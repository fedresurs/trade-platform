import os
from pydantic import BaseSettings, validator, MongoDsn, Field
from pathlib import Path


BASE_DIR = Path(os.path.dirname(os.path.dirname(__file__)))


class Base(BaseSettings):
    class Config:
        env_file = os.path.join(BASE_DIR.parent, '.env')


class SettingsMongo(Base):
    protocol: str = "mongodb"
    username: str = Field(..., env="MONGO_INITDB_ROOT_USERNAME")
    password: str = Field(..., env="MONGO_INITDB_ROOT_PASSWORD")
    host: str = '127.0.0.1'
    port: int = 27017
    path: str = 'admin'
    db_name: str
    dns: MongoDsn = None

    @validator("dns", pre=True)
    def build_dsn(cls, v, values) -> str:
        protocol = values['protocol']
        username = values['username']
        password = values['password']
        host = values['host']
        port = values['port']
        path = values['path']

        return f"{protocol}://{username}:{password}@{host}:{port}/{path}"

    class Config:
        env_prefix = "MONGO_"


class BrowserSettings(Base):
    headless: bool = False
    slow_mo: int = 1000
    timeout: int = 20 * 1000


class Settings(Base):
    base_dir: Path = BASE_DIR
    browser: BrowserSettings = BrowserSettings()
    mongo: SettingsMongo = SettingsMongo()
