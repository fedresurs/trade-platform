import abc
from typing import Optional
from pathlib import Path
import orjson
from schema import Platform


class BaseStorage:

    @abc.abstractmethod
    def save_state(self, state: dict) -> None:
        """Сохранить состояние в постоянное хранилище"""
        pass

    @abc.abstractmethod
    def retrieve_state(self) -> dict:
        """Загрузить состояние локально из постоянного хранилища"""
        pass


class JsonFileStorage(BaseStorage):

    def __init__(self, file_path: Optional[Path] = None):
        self.file_path = file_path
        Path(self.file_path).touch()

    def save_state(self, state: dict) -> None:
        old_state = self.retrieve_state()
        new_state = {**old_state, **state}
        with open(self.file_path, 'wb') as file:
            current = orjson.dumps(
                new_state,
                option = orjson.OPT_SERIALIZE_NUMPY | orjson.OPT_INDENT_2
            )
            file.write(current)

    def retrieve_state(self) -> dict:
        with open(self.file_path, 'rb') as f:
            content = f.read()
            if not content:
                return {}
            return orjson.loads(content)


class State:

    def __init__(self, storage: BaseStorage):
        self.storage = storage

    def get(self, key: str):
        state = self.storage.retrieve_state()
        if key in state.keys():
            return Platform(**state[key])
        return None

    def set_state(self, key: str, platform: Platform) -> None:
        self.storage.save_state({key: platform.dict()})
