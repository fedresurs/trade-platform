from core.settings import Settings, BASE_DIR
from core.state import JsonFileStorage, State


settings: Settings = Settings()
storage = JsonFileStorage(BASE_DIR / 'state.json')
state = State(storage)
