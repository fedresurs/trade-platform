import asyncio
from trade_platform.start import worker


# def cli(name):
#     names = ['alfalot', 'arbitat', 'bankrupt_tender', 'bepspb', 'centerr', 'electro_torgi', 'etp_bankrotstvo', 'etpu',
#              'etpugra', 'gloriaservice', 'meta_invest', 'propertytrade', 'tendergarant', 'torgibankrot', 'uralbidin',
#              'utender', 'utpl', 'zakazrf']
#
#     asyncio.run(worker(name))


import click

@click.command()
@click.option("--platform", type = str, required = True, help="")
def cli(name: float) -> None:
    print(name)


if __name__ == "__main__":
    cli()