import re
from schema import Platform
from playwright.async_api import Page
from parsel import Selector


class SpiderPurchase:

    def __init__(self, platform: Platform, page: Page):
        self.page = page
        self.platform = platform
        self.counter_item = 0

    @property
    async def selector(self) -> Selector:
        content = await self.page.content()
        content = re.sub(r"[\n\t\r]", "", content)
        content = re.sub("\s+", ' ', content)
        content = re.sub("\xa0", ' ', content)
        return Selector(text = content)

    @property
    async def last_page(self):
        selector = await self.selector
        last_page = selector.xpath('//td[@class="pager"][1]//text()').getall()
        last_page = last_page[-1]
        return last_page if last_page == '>>' else int(last_page)

    async def start(self):
        await self.page.goto(self.platform.start_url)

    async def parse(self, current_page: int = 1):

        last_page = await self.last_page

        while last_page == '>>' or last_page >= current_page:
            last_page = await self.last_page

            if current_page >= self.platform.limit_page:
                break

            async for item in self.parse_table():
                self.counter_item += 1
                yield item

            if current_page == last_page:
                break

            if current_page % 10 == 0:
                await self.page.click(f'//td[@class="pager"]/a[text()=">>"]')
                current_page += 1
                continue

            current_page += 1
            await self.page.click(f'//td[@class="pager"]/a[text()="{current_page}"]')

    async def parse_table(self):
        selector = await self.selector

        for tr in selector.xpath('//tr[@class="gridRow"]'):

            item = {
                'trade_id': tr.xpath('./td[1]/a/text()').get(),
                'lot_url': tr.xpath('./td[4]/a[@class="tip-lot"]/@href').get(),
                'description': tr.xpath('./td[4]/a[@class="tip-lot"]/text()').get(),
                'even_date': tr.xpath('./td[8]/text()').get(),
                'status': tr.xpath('./td[9]/text()').get(),
                'trade_type': tr.xpath('./td[11]/text()').get(),
            }
            yield item

    async def stop(self):
        await self.page.close()


# async def crawl(platform: PlatformModel):
#     total_updated = 0
#     total_inserted = 0
#     total_pass = 0
#
#     logger.info(f'start crawl {platform.name}')
#
#     browser_settings = {
#         'slow_mo': platform.slow_mo
#     }
#
#     spider_purchase = await SpiderPurchase(platform, browser_settings).create()
#     await spider_purchase.start()
#
#     async for item in spider_purchase.parse():
#         item['lot_url'] = platform.main_url + item.pop('lot_url')
#         item['platform_name'] = platform.name
#
#         result = await save_lot(item)
#
#         if result is None:
#             total_pass += 1
#
#         if result == 'updated':
#             total_updated += 1
#
#         if result == 'inserted':
#             total_inserted += 1
#
#     logger.info(f'total pass : {total_pass}')
#     logger.info(f'total updated : {total_updated}')
#     logger.info(f'total inserted : {total_inserted}')
#
#     await spider_purchase.stop()
#
#
# async def main():
#     await init_db()
#
#     platforms = PlatformModel.find(PlatformModel.status == 'enabled')
#
#     async for platform in platforms:
#         platform.limit_page = 5
#         await crawl(platform)
#
#
# asyncio.run(main())
