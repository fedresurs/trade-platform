import asyncio
import datetime
import random
from models.model import Platform, Lot, RawInfo
from database.mongo import init_db, db
from typing import List
from aiohttp import ClientSession
from playwright.async_api import async_playwright
from parsel import Selector
from itertools import product
from collections import ChainMap
from beanie.odm.operators.update.general import Set
from dateparser import parse as dt_parse
from helps import get_selector


async def get_cookies(url):
    dict_cookies = {}

    async with async_playwright() as p:
        browser = await p.chromium.launch()
        context = await browser.new_context()
        page = await context.new_page()
        await page.goto(url)

        cookies = await context.cookies()
        for cookie in cookies:
            dict_cookies[cookie['name']] = cookie['value']

        await browser.close()

    return dict_cookies


def parse_tabel(table: Selector) -> dict:
    data = {}
    for tr, num_cell in product(table.xpath('.//tr'), [1, 3]):
        k = ''.join(tr.xpath(f"./td[{num_cell}]//text()").getall())
        v = ''.join(tr.xpath(f"./td[{num_cell + 1}]//text()").getall())
        if k and v and k != ' ' and k != '\xa0':
            data[k] = v
    return data


async def parser(selector: Selector):
    tables_info = selector.xpath('//legend[contains(text(), "Информация о")]/following-sibling::table')
    data = [parse_tabel(table) for table in tables_info]

    tables_price = selector.xpath('//legend[contains(text(), "Интервалы снижения цены")]/following-sibling::div/table')
    intervals = []

    if tables_price:

        for tr in tables_price.xpath('.//tr[@class="gridRow"]'):
            interval = {
                "start": dt_parse(tr.xpath('./td[1]/text()').get()),
                "start_bid": dt_parse(tr.xpath('./td[2]/text()').get()),
                "end_bid": dt_parse(tr.xpath('./td[3]/text()').get()),
                "end": dt_parse(tr.xpath('./td[4]/text()').get()),
                "price_drop": tr.xpath('./td[5]/text()').get(),
                "deposit": tr.xpath('./td[6]/text()').get(),
                "price": tr.xpath('./td[7]/text()').get()
            }
            intervals.append(interval)

    return data, intervals


async def fetch(lot: Lot, session: ClientSession):

    async with session.get(lot.lot_url) as response:

        if response.ok:
            selector = await get_selector(content=await response.text())
            data, intervals = await parser(selector)
            data = dict(ChainMap(*data))

            await RawInfo.find_one(RawInfo.lot_id == lot.id).upsert(
                Set({
                    RawInfo.data: data,
                    RawInfo.content: selector.get(),
                    RawInfo.price_intervals: intervals,
                }),
                on_insert=RawInfo(
                    lot_id=lot.id,
                    lot_url=lot.lot_url,
                    data=data,
                    content=selector.get(),
                    price_intervals=intervals
                ))
    lot.visited = datetime.datetime.now()
    await lot.save()
    print(lot.id, 'ok')
    await asyncio.sleep(random.randint(1, 3))


async def bound_fetch(sem, url, session):
    async with sem:
        await fetch(url, session)


async def start(batch_lots: List[Lot], cookies: dict):

    semaphore = asyncio.Semaphore(5)

    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:108.0) Gecko/20100101 Firefox/108.0',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
        'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
    }
    async with ClientSession(cookies=cookies, headers=headers) as session:
        tasks = [
            asyncio.ensure_future(bound_fetch(semaphore, lot, session))
            for lot in batch_lots
        ]
        responses = asyncio.gather(*tasks)
        await responses


async def main(name_platform, batch_lots):

    platform = await Platform.find_one(Platform.name == name_platform)
    cookies = await get_cookies(platform.main_url)
    await start(batch_lots, cookies)


async def run():

    await init_db()

    for name_platform in await db['trading_platform'].distinct('name'):
        not_visited = Lot.find(Lot.platform_name == name_platform, Lot.visited == None)
        not_visited_count = 1
        while not_visited_count != 0:
            not_visited_count = await not_visited.count()
            print(name_platform, not_visited_count)
            batch_lots = await not_visited.limit(1000).to_list()
            await main(name_platform, batch_lots)


asyncio.run(run())
