from contextlib import asynccontextmanager
from playwright.async_api import PlaywrightContextManager
from core import settings
from schema import Platform
from core import state


@asynccontextmanager
async def Browser(name: str, platform: Platform):
    manager = PlaywrightContextManager()
    playwright = await manager.start()
    try:
        platform.process = True
        state.set_state(name, platform)

        if platform.slow_mo:
            settings.browser.slow_mo = platform.slow_mo

        browser = await playwright.chromium.launch(
            **settings.browser.dict()
        )

        context = await browser.new_context()
        yield context
    finally:
        platform.process = False
        state.set_state(name, platform)
        await context.close()
